# Add some users and groups
# The nsadmin user is mostly so we have a human readable owner for files created in local zones by that user.
if  ! grep -q '^web::' /etc/group; then
    groupadd -g 5001 web
fi

if  ! id nsadmin 2>/dev/null; then
    useradd -g web -u 5001 -d /export/home/nsadmin nsadmin
fi

# the zfsmgr user allows us to perform ZFS operations without logging in as root
if  ! id zfsmgr 2>/dev/null ; then
    useradd -u 802 -g 10 -d /export/home/zfsmgr -m -s /bin/pfbash -P "ZFS File System Management" zfsmgr
fi
if ! grep -q /opt/ooce/bin ~zfsmgr/.profile; then
    echo "export PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin" >> ~zfsmgr/.profile
    echo "export PATH=\$PATH:/opt/ooce/bin" >> ~zfsmgr/.profile
fi
grep -q /usr/local/bin ~zfsmgr/.profile || echo "export PATH=\$PATH:/usr/local/bin" >> ~zfsmgr/.profile
grep -q .profile ~zfsmgr/.bashrc || echo "source ~/.profile" >> ~zfsmgr/.bashrc

if ! grep -q /opt/ooce/bin ~root/.profile; then
    echo "export PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin" >> ~root/.profile
    echo "export PATH=\$PATH:/opt/ooce/bin" >> ~root/.profile
fi
grep -q /usr/local/bin ~root/.profile || echo "export PATH=\$PATH:/usr/local/bin" >> ~root/.profile
grep -q .profile ~root/.bashrc || echo "source ~/.profile" >> ~root/.bashrc

if ! grep -q /opt/ooce/bin ~/.profile; then
    echo "export PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin" >> ~/.profile
    echo "export PATH=\$PATH:/opt/ooce/bin" >> ~/.profile
fi
grep -q /usr/local/bin ~/.profile || echo "export PATH=\$PATH:/usr/local/bin" >> ~/.profile
grep -q .profile ~/.bashrc || echo "source ~/.profile" >> ~/.bashrc

# Add mbuffer for faster zfs send/receive
pkg install mbuffer gnupg zadm zrepl

mkdir -p ~/.ssh
mkdir -p ~root/.ssh
cp lgnsw-general-read* ~/.ssh/
cp lgnsw-general-read* ~root/.ssh/
cat ssh-config.txt>> ~/.ssh/config
cat ssh-config.txt>> ~root/.ssh/config
cat known_hosts >> ~/.ssh/known_hosts
cat known_hosts >> ~root/.ssh/known_hosts
chmod -R go-rw ~root/.ssh
chmod -R go-rw ~/.ssh
chown -R $USER ~/.ssh
chown -R root ~root/.ssh

# Adjust to correct time and start ntp service
ntpdig -S -s -M 128 0.omnios.pool.ntp.org
svcadm enable svc:/network/ntp:default

echo ""
echo "========== NOTE! =========="
echo "If your local network blocks ntp request to external ntp servers, make the following adjustments."
echo "ntpdig -S -s -M 128 <ip of a local time server> "
echo "echo \"server <local time server IP>\" >> /etc/inet/ntp.conf"
echo "svcadm restart svc:/network/ntp:default"
echo ""
echo "Update your path in this shell session with '. ~/.profile'"

