#!/usr/bin/bash

pkg install mutt 

cp /etc/opt/ooce/mutt/Muttrc /etc/opt/ooce/mutt/Muttrc.original
sed -r -i 's@# (set folder=)("~/Mail")@\1"~/Maildir"@' /etc/opt/ooce/mutt/Muttrc 
sed -r -i 's@# (set mbox=)("~/mbox")@\1"~/Maildir"@' /etc/opt/ooce/mutt/Muttrc 
sed -r -i 's@# (set mbox_type=)(mbox)@\1Maildir@' /etc/opt/ooce/mutt/Muttrc 
sed -r -i 's@# (set postponed=)("~/postponed")@\1"~/+.Drafts"@' /etc/opt/ooce/mutt/Muttrc 
sed -r -i 's@# (set record=)("~/sent")@\1"~/+.sent"@' /etc/opt/ooce/mutt/Muttrc 
sed -r -i 's@# (set spoolfile=)("")@\1"~/Maildir"@' /etc/opt/ooce/mutt/Muttrc
