#!/usr/bin/bash

def_hostname=`hostname`
echo "Host Name (not FQDN) to be used for postfix config (defaults to $def_hostname):"
read hostname
if [ -z "$hostname" ]; then
    hostname=$def_hostname
fi

def_domain=`grep domain /etc/resolv.conf |cut -f2 -d" "`
echo "Domain name for postfix config (defaults to $def_domain):"
read domainname
if [ -z "$domainname" ]; then
    domainname=$def_domain
fi

echo "FQDN or IP address of the relayhost (blank for none):"
read relay_host

pkg install ooce/network/smtp/postfix

cp /etc/opt/ooce/postfix/main.cf /etc/opt/ooce/postfix/main.cf.original
sed  -i '/#myhostname = virtual.domain.tld/a\
myhostname = '${hostname}.${domainname}'' /etc/opt/ooce/postfix/main.cf
sed -r -i 's/#(home_mailbox = Maildir\/)/\1/' /etc/opt/ooce/postfix/main.cf
sed -r -i 's/#(relayhost =) uucphost$/\1 '$relay_host'/' /etc/opt/ooce/postfix/main.cf

svcadm enable -s svc:/network/smtp/postfix:default
/usr/sbin/newaliases
