#!/usr/bin/bash

if [ ! -f /opt/local/bin/pkgin ]; then
    BOOTSTRAP_TAR="bootstrap-trunk-x86_64-20230313.tar.gz"

    # Download the bootstrap kit to the current directory.
    curl -O https://pkgsrc.smartos.org/packages/SmartOS/bootstrap/${BOOTSTRAP_TAR}

    # Verify PGP signature.  This step is optional, and requires gpg.
    curl -O https://pkgsrc.smartos.org/packages/SmartOS/bootstrap/${BOOTSTRAP_TAR}.asc
    curl -sS https://pkgsrc.smartos.org/pgp/8254B861.asc | gpg --import
    gpg --verify ${BOOTSTRAP_TAR}{.asc,}

    # Install bootstrap kit to /opt/local
    tar -zxpf ${BOOTSTRAP_TAR} -C /
fi

if ! grep -q /opt/local/bin ~/.profile; then
    echo "export PATH=\$PATH:/opt/local/bin:/opt/local/sbin" >> ~/.profile
    echo "NOTE!"
    echo "Update your path in this shell session with '. ~/.profile'"
fi

grep -q /opt/local/bin ~root/.profile || echo "export PATH=\$PATH:/opt/local/bin:/opt/local/sbin" >> ~root/.profile

if grep -q zfsmgr /etc/passwd; then
    grep -q /opt/local/bin ~zfsmgr/.profile ||  echo "export PATH=\$PATH:/opt/local/bin:/opt/local/sbin" >> ~zfsmgr/.profile
fi
