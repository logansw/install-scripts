#
# Copy and paste the lines below to upgrade to the latest bootstrap.  This
# will overwrite the following files:
#
#	/opt/local/etc/mk.conf
#	/opt/local/etc/pkg_install.conf
#	/opt/local/etc/pkgin/repositories.conf
#	/opt/local/etc/gnupg/pkgsrc.gpg
#
UPGRADE_TAR="bootstrap-trunk-x86_64-20230313-upgrade.tar.gz"
UPGRADE_SHA="13a31cab4787e2c45ed645872971d2ad46fe66ad"

# Download the upgrade kit to the current directory.
curl -O https://pkgsrc.smartos.org/packages/SmartOS/bootstrap-upgrade/${UPGRADE_TAR}

# Verify the SHA1 checksum.
[ "${UPGRADE_SHA}" = "$(/bin/digest -a sha1 ${UPGRADE_TAR})" ] || echo "ERROR: checksum failure"

# Verify PGP signature.  This step is optional, and requires gpg.
#curl -O https://pkgsrc.smartos.org/packages/SmartOS/bootstrap-upgrade/${UPGRADE_TAR}.asc
#curl -sS https://pkgsrc.smartos.org/pgp/8254B861.asc | gpg2 --import
#gpg2 --verify ${UPGRADE_TAR}{.asc,}

# Unpack upgrade kit to /opt/local
tar -zxpf ${UPGRADE_TAR} -C /

# Ensure you are running the latest package tools.
pkg_add -U pkg_install pkgin libarchive

# Clean out any old packages signed with the previous key.
pkgin clean

# Upgrade all packages.
pkgin -y upgrade
