#!/usr/bin/bash

pgver=14
geosver=3.8.0
projver=5.0.1
jsonver=0.13.1-20180305
gdalver=2.2.4
postgisver=3.1.5

pkg install libxml2 cmake pkg-config tiff
pkgin -y install protobuf-c-1.3.3

export LDFLAGS=" -R/usr/local/lib -L/usr/local/lib -R/usr/local/postgres/$pgver/lib -L/usr/local/postgres/$pgver/lib"

cd /usr/local/src
if [ ! -f "/usr/local/src/geos-${geosver}.tar.bz2" ]; then
    wget http://download.osgeo.org/geos/geos-${geosver}.tar.bz2
fi
if [ ! -d "geos-${geosver}" ]; then
    tar xjf geos-${geosver}.tar.bz2
    cd geos-${geosver}
    #echo "geos $geosver configure"
    #./configure "CFLAGS=-m64" "CXXFLAGS=-m64" "LDFLAGS=-m64" > config.out 2>&1
    mkdir _build
    cd _build
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ..
    echo "geos $geosver compile"
    gmake > compile.out 2>&1
    echo "geos $geosver install"
    gmake install > install.out 2>&1
fi


cd /usr/local/src
if [ ! -f "/usr/local/src/proj-${projver}.tar.gz" ]; then
    wget http://download.osgeo.org/proj/proj-${projver}.tar.gz
fi
if [ ! -d "proj-${projver}" ]; then
    tar xzf proj-${projver}.tar.gz
    cd proj-${projver}
    echo "proj $projver configure"
    ./configure "CFLAGS=-m64" "CXXFLAGS=-m64" "LDFLAGS=-m64" > config.out 2>&1
    echo "proj $projver compile"
    gmake > compile.out 2>&1
    echo "proj $projver install"
    gmake install > install.out 2>&1
fi


cd /usr/local/src
if [ ! -f "/usr/local/src/json-c-${jsonver}.tar.gz" ]; then
    wget https://github.com/json-c/json-c/archive/refs/tags/json-c-${jsonver}.tar.gz
fi
if [ ! -d "json-c-${jsonver}" ]; then
    tar xzf json-c-${jsonver}.tar.gz
    cd json-c-json-c-${jsonver}
    echo "json-c-$jsonver configure"
    ./configure "CFLAGS=-m64" "CXXFLAGS=-m64" "LDFLAGS=-m64" > config.out 2>&1
    echo "json-c $jsonver compile"
    gmake > compile.out 2>&1
    echo "json-c $jsonver install"
    gmake install > install.out 2>&1
fi


cd /usr/local/src
if [ ! -f "/usr/local/src/gdal-${gdalver}.tar.gz" ]; then
    wget http://download.osgeo.org/gdal/${gdalver}/gdal-${gdalver}.tar.gz
fi
if [ ! -d "gdal-${gdalver}" ]; then
    tar xzf gdal-${gdalver}.tar.gz
    cd gdal-${gdalver}
    echo "gdal $gdalver configure"
    ./configure CFLAGS=-m64 CXXFLAGS=-m64 LDFLAGS="-m64 -L/usr/local/postgres/$pgver/lib -R/usr/local/postgres/$pgver/lib" --with-libjson-c=/usr/local --with-hide-internal-symbols=yes > config.out 2>&1
    echo "gdal $gdalver compile"
    gmake > compile.out 2>&1
    echo "gdal $gdalver install"
    gmake install > install.out 2>&1
fi


cd /usr/local/src
if [ ! -f "/usr/local/src/postgis-${postgisver}.tar.gz" ]; then
    wget https://download.osgeo.org/postgis/source/postgis-${postgisver}.tar.gz
fi
if [ ! -d "postgis-${postgisver}" ]; then
    tar xzf postgis-${postgisver}.tar.gz
    cd postgis-${postgisver}
    echo "posgis $postgisver configure"
    ./configure CPPFLAGS=-I/opt/local/include CFLAGS=-m64 CXXFLAGS=-m64 LDFLAGS="-m64 -L/usr/local/postgres/$pgver/lib -R/usr/local/postgres/$pgver/lib -L/usr/local/lib -R/usr/local/lib -L/opt/local/lib -R/opt/local/lib" --with-geosconfig=/usr/local/bin/geos-config --with-projdir=/usr/local/ --with-jsondir=/usr/local --without-protobuf --with-gdalconfig=/usr/local/bin/gdal-config --with-pgconfig=/usr/local/postgres/$pgver/bin/pg_config > config.out 2>&1
    echo "posgis $postgisver compile"
    gmake > compile.out 2>&1
    echo "posgis $postgisver install"
    gmake install > install.out 2>&1
fi

