#!/usr/bin/bash

script_dir="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

def_pg_version=`which psql |cut -f5 -d/`
echo "Enter the Postgres version (major version only) to compile with."
echo "   Defaults to ${def_pg_version}."
read pg_version
if [ -z "$pg_version" ]; then
    pg_version=$def_pg_version
fi

mkdir -p /usr/local/src
cd /usr/local/src
if [ ! -d install-ns ]; then
    git clone -b release/3 -o lgnsw https://bbs.logansw.com/scm/tools/install-ns.git
    git -C install-ns remote set-url --push lgnsw https://bbs.logansw.com/scm/tools/install-ns.git
fi

# Set the postgres version in the naviserver install script
sed -i "s/^version_postgres=.*/version_postgres=${pg_version}/" /usr/local/src/install-ns/install-ns.sh

cd install-ns
./install-ns.sh build


source /usr/local/ns/lib/nsConfig.sh

mkdir -p /web/sites
mkdir -p /web/etc/certs
mkdir -p /web/bin
mkdir -p /web/logs/error
mkdir -p /web/logs/access
chown -R ${ns_user}:${ns_group} /web
chmod -R g+ws /web

su nsadmin -c "git clone -o lgnsw https://bbs.logansw.com/scm/oacspub/openacs-core-l-5-10.git /web/sites/openacs"
su nsadmin -c "cp /web/sites/openacs/etc/ns-example-config.tcl /web/etc/openacs.tcl"
su nsadmin -c "cp /web/sites/openacs/etc/certs/testcert.pem /web/etc/certs/"

su - postgres -c "/usr/local/postgres/${pg_version}/bin/psql postgres -f $script_dir/db-globals.sql"

su - nsadmin -c "/usr/local/postgres/${pg_version}/bin/createdb openacs"
su - nsadmin -c "/usr/local/postgres/${pg_version}/bin/psql -c 'alter database openacs set search_path = public,vault,usurf,mfg,eoffice,lsw,warehouse;' openacs "

ns_homedir=$( getent passwd "$ns_user" | cut -d: -f6 )
mkdir -p ${ns_homedir}/.ssh
cp /usr/local/install-scripts/lgnsw-general-read* ${ns_homedir}/.ssh/
cat /usr/local/install-scripts/ssh-config.txt>> ${ns_homedir}/.ssh/config
cat /usr/local/install-scripts/known_hosts >> ${ns_homedir}/.ssh/known_hosts
chown -R ${ns_user}:${ns_group} ${ns_homedir}/.ssh
chmod -R go-rw ${ns_homedir}/.ssh

cd /usr/local/oacs-util/naviserver-svc
/usr/local/oacs-util/naviserver-svc/install-svc.sh --site=openacs --user=nsadmin

