#!/usr/bin/bash

pg_ver=14.6

set -- `echo $pg_ver | tr '.' ' '`
pg_maj=$1
pg_min=$2
pg_rc=$3

mkdir -p /usr/local/src
cd /usr/local/src
if [ ! -f install-postgres ]; then
    git clone -o lgnsw -b release/PG11plus https://bbs.logansw.com/scm/tools/install-postgres.git
    git -C install-postgres remote set-url --push lgnsw https://bbs.logansw.com/scm/tools/install-postgres.git
fi

cd install-postgres/
./install_postgres.sh --version=$pg_ver --subdir=$pg_maj --path=/usr/local/postgres --conf-inc-dir=/usr/local/etc
./setup_pg_db.sh
./configure_pg_smf.sh
cd /usr/local/postgres
rm $pg_maj
ln -s $pg_ver $pg_maj


python3 -m pip install Faker

cd /usr/local/src
git clone https://gitlab.com/dalibo/postgresql_faker.git
cd postgresql_faker
sed -i "s/python=python3.6/python=python3.9/" /usr/local/src/postgresql_faker/Makefile
gmake
gmake install

sleep 3
svcs -a |grep postgresql
echo "Start postgres with: svcadm enable svc:/application/database/postgresql${pg_maj}:v${pg_maj}${pg_min}_64bit"

