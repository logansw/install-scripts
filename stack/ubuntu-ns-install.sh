apt -y update &&\
     apt -y upgrade &&\
     DEBIAN_FRONTEND='noninteractive' apt -y install vim bash-completion wget curl libcurl4 unzip zip htmldoc openjdk-17-jre-headless imagemagick jhead python3-pip python3-dev sshpass ripmime nmap telnet python3-ldap3 lsb-release xlsx2csv &&\
     apt -y clean all


wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - &&\
     echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | tee  /etc/apt/sources.list.d/pgdg.list &&\
     apt -y update &&\
     apt -y install postgresql-client-14

mkdir -p /usr/local &&\
     wget https://exiftool.org/Image-ExifTool-12.40.tar.gz &&\
     tar xzf Image-ExifTool-12.40.tar.gz -C /usr/local/ &&\
     mv /usr/local/Image-ExifTool-12.40 /usr/local/Image-ExifTool &&\
     rm Image-ExifTool-12.40.tar.gz

groupadd -g 5001 web &&\
    useradd -g 5001 -u 5001 -d /export/home/nsadmin -m nsadmin -s /bin/bash


apt -y install build-essential pkg-config git libssl-dev zlib1g-dev libpq-dev autoconf

mkdir -p /usr/local/src &&\
    cd /usr/local/src &&\
    git clone -b release/3 -o lgnsw https://bbs.logansw.com/scm/tools/install-ns.git &&\
    cd install-ns &&\
    ns_group=web with_postgres=0 bash ./install-ns.sh build

cd /usr/local/src &&\
    git clone https://github.com/kpaskett/libtclpy.git &&\
    cd libtclpy &&\
    autoconf &&\
    ./configure --prefix=/usr/local/ns --with-tcl=/usr/local/ns/lib --with-python-version=3.8 &&\
    make &&\
    make install
# ignore the install-doc error

cd /usr/local/src &&\
    wget https://core.tcl-lang.org/tcltls/uv/tcltls-1.7.22.tar.gz &&\
    tar xzf tcltls-1.7.22.tar.gz &&\
    cd tcltls-1.7.22 &&\
    ./configure --with-tcl=/usr/local/ns/lib
make &&\
    make install &&\
    /usr/bin/install -c -d /usr/local/ns/lib/tcltls1.7.22 &&\
    /usr/bin/install -c tcltls.so /usr/local/ns/lib/tcltls1.7.22 &&\
    /usr/bin/install -c -m 644 pkgIndex.tcl /usr/local/ns/lib/tcltls1.7.22

cd /usr/local/src &&\
    git clone https://bitbucket.org/naviserver/revproxy.git &&\
    cd revproxy &&\
    make install


# The following are only needed if you want to connect to an Oracle database

cd /usr/local/src
apt -y install libaio1 libaio-dev

wget https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-basic-linux.x64-21.1.0.0.0.zip &&\
    unzip instantclient-basic-linux.x64-21.1.0.0.0.zip -d /opt/oracle
wget https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-sdk-linux.x64-21.1.0.0.0.zip &&\
    unzip instantclient-sdk-linux.x64-21.1.0.0.0.zip -d /opt/oracle
# Comment the next two lines if you don't need/want sqlplus installed in the container
wget https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-sqlplus-linux.x64-21.1.0.0.0.zip &&\
    unzip instantclient-sqlplus-linux.x64-21.1.0.0.0.zip -d /opt/oracle
sh -c "echo /opt/oracle/instantclient_21_1 > /etc/ld.so.conf.d/oracle-instantclient.conf" &&\
    ldconfig

git clone https://bbs.logansw.com/scm/oacs/nsoracle.git &&\
    cd nsoracle/ &&\
    make ORA_CFLAGS="-I/opt/oracle/instantclient_21_1/sdk/include -L/opt/oracle/instantclient_21_1" &&\
    make install

