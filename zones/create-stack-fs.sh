#!/usr/bin/bash

pgver=14

def_stack=lsw
echo "What stack is the zone being created under (defaults to $def_stack)?"
read stackname
if [ -z $stackname ]; then
    stackname=$def_stack
fi
echo

def_type=p
echo "Create filesystems for (p)ostgres, (n)aviserver, or (b)oth/combined?"
echo "enter p, n or b. (defaults to p)"
read ztype
if [ -z $ztype ]; then
    ztype=$def_type
fi
echo


zonesfs=$(zfs list -H -o name,mountpoint |grep -w "/zones$" |cut -f1)
spacefs=$(zfs list -H -o name,mountpoint |grep -w "/zonespace$" |cut -f1)
# Make sure zfs compression is on

zfs set compression=lz4 $zonesfs
zfs set compression=lz4 $spacefs

stackbase=${spacefs}/${stackname}

echo "$stackbase"

if [[ "${ztype}" == b ]] || [[ "${ztype}" == p ]]; then
    if [[ "${ztype}" == p ]]; then
        zspacebase=${stackbase}/pg
    else
        zspacebase=${stackbase}
    fi
  
    zfs create -p $zspacebase/homes
    zfs create -p $zspacebase/pg${pgver}logs
    zfs create -p $zspacebase/pg${pgver}/wal
    zfs create -p $zspacebase/local
fi

if [[ "${ztype}" == b ]] || [[ "${ztype}" == n ]]; then
    if [[ "${ztype}" == n ]]; then
        zspacebase=${stackbase}/ns
    else
        zspacebase=${stackbase}
    fi
  
    zfs create -p $zspacebase/homes
    zfs create -p $zspacebase/local
    zfs create -p $zspacebase/web
    zfs create -p $zspacebase/logs
    zfs create -p $zspacebase/files
fi





