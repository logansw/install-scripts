#!/usr/bin/bash

pgver=14

def_stack=lsw
echo "What stack is the zone being created under (defaults to $def_stack)?"
read stackname
if [ -z $stackname ]; then
    stackname=$def_stack
fi
echo

def_dir=pg
echo "What is the zone directory under the stack (defaults to $def_dir)?"
read dirname
if [ -z $dirname ]; then
    dirname=$def_dir
fi
echo

def_zone=${stackname}${dirname}
echo "What is the name of the zone being created (defaults to $def_zone)?"
read zonename
if [ -z $zonename ]; then
    zonename=$def_zone
fi
echo

echo
echo "Enter a zadm JSON zone template file."
read zone_template
if [ ! -f "$zone_template" ]; then
    echo "$zone_template does not exist."
    exit
fi

def_brand=lipkg
echo "Enter a zadm zone type/brand defaults to $def_brand"
read zone_brand
if [ -z $zone_brand ]; then
    zone_brand=$def_brand
fi

zonesfs=$(zfs list -H -o name,mountpoint |grep -w "zones$" |cut -f1)
spacefs=$(zfs list -H -o name,mountpoint |grep -w "zonespace$" |cut -f1)
# Make sure zfs compression is on
zfs set compression=lz4 $zonesfs
zfs set compression=lz4 $spacefs

git clone -b release/OmniOSce151038-PG14 -o lgnsw ssh://git@bbs.logansw.com:7999/tools/install-scripts.git /zonespace/${stackname}/${dirname}/local/install-scripts
git -C /zonespace/${stackname}/${dirname}/local/install-scripts remote set-url lgnsw --push https://bbs.logansw.com/scm/tools/install-scripts.git

zadm create -b $zone_brand $zonename < $zone_template
#cp /etc/TIMEZONE /zones/${zonename}/root/etc/

zadm start $zonename
