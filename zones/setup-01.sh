#!/usr/bin/bash

pgver=14

pkg install gnupg git build-essential

mkdir -p /root/.ssh
cp ../lgnsw-general-read* /root/.ssh/
cat ../ssh-config.txt>> /root/.ssh/config
cat ../known_hosts >> /root/.ssh/known_hosts
chmod -R go-rw /root/.ssh

cp bashrc /etc/bash/
cp dot_bash_profile /root/.bash_profile
cp dot_bash_profile /etc/skel/.bash_profile
cp dot_bashrc /root/.bashrc
cp dot_bashrc /etc/skel/.bashrc
sed -r -i 's/<<<pgver>>>/'$pgver'/' /etc/bash/bashrc

. dot_bashrc

../joyent-install.sh
pkg install openjdk11

echo ""
echo "NOTE! do the following before proceeding"
echo "get paths up to date by running '. ~/.bashrc'"
