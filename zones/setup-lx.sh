#!/usr/bin/bash

pgver=14

apt -y install gnupg git

mkdir -p /root/.ssh
cp ../lgnsw-general-read* /root/.ssh/
cat ../ssh-config.txt>> /root/.ssh/config
cat ../known_hosts >> /root/.ssh/known_hosts
chmod -R go-rw /root/.ssh

mkdir -p /etc/bash
cp bashrc /etc/bash/
cp dot_bashrc /root/.bashrc
cp dot_bashrc /etc/skel/.bashrc
sed -r -i '/<<<pgver>>>/d' /etc/bash/bashrc

git clone -b release/3 -o lgnsw ssh://git@bbs.logansw.com:7999/tools/git-sync.git /usr/local/git-sync
git -C /usr/local/git-sync remote set-url --push lgnsw https://bbs.logansw.com/scm/tools/git-sync.git
git clone -b release/1.0 -o lgnsw ssh://git@bbs.logansw.com:7999/tools/oacs-util.git /usr/local/oacs-util
git -C /usr/local/oacs-util remote set-url --push lgnsw https://bbs.logansw.com/scm/tools/oacs-util.git

. dot_bashrc

echo ""
echo "NOTE! do the following before proceeding"
echo "get paths up to date by running '. ~/.bashrc'"
