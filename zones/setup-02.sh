#!/usr/bin/bash

git clone -b release/3 -o lgnsw https://bbs.logansw.com/scm/tools/git-sync.git /usr/local/git-sync
git clone -b release/1.0 -o lgnsw https://bbs.logansw.com/scm/tools/oacs-util.git /usr/local/oacs-util
git -C /usr/local/oacs-util remote set-url --push lgnsw https://bbs.logansw.com/scm/tools/oacs-util.git

#pkgin -y install wkhtmltopdf
pkgin -y install jhead
pkgin -y install poppler
pkgin -y install poppler-utils
pkgin -y install dejavu-ttf

pkg install build-essential

pkg install imagemagick \
zlib \
wget \
curl \
compress/zip \
compress/unzip

wget https://exiftool.org/Image-ExifTool-12.07.tar.gz
gtar xvf Image-ExifTool-12.07.tar.gz -C /usr/local/
ln -s -r /usr/local/Image-ExifTool-12.07 /usr/local/Image-ExifTool

wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install setuptools --upgrade --force
