#!/usr/bin/bash

# Install perl and modules needed for sanoid
yes | cpan Config::IniFiles
yes | cpan Capture::Tiny

cd /usr/local
git clone https://github.com/jimsalterjrs/sanoid.git
mkdir -p /usr/local/etc/sanoid
cp sanoid/sanoid.conf sanoid/sanoid.defaults.conf /usr/local/etc/sanoid/

echo "To enable sanoid, add the following to the zfsmgr user's crontab."
echo "# * * * * * TZ=UTC /usr/local/sanoid/sanoid --cron --configdir=/usr/local/etc/sanoid > /dev/null 2>&1"

cd /usr/local
git clone -b release/1.3 -o lgnsw https://bbs.logansw.com/scm/tools/zfs-sync /usr/local/zfs-sync
mkdir -p /usr/local/etc/zfs-sync
mkdir -p /usr/local/bin
cp /usr/local/zfs-sync/sample.syncspec /usr/local/etc/zfs-sync/
cp /usr/local/zfs-sync/*.sh /usr/local/bin/

if ! grep -q /usr/local/bin ~zfsmgr/.profile; then 
    echo "export PATH=\$PATH:/usr/local/bin" >> ~zfsmgr/.profile 
fi
if ! grep -q /usr/local/bin ~/.profile; then
    echo "export PATH=\$PATH:/usr/local/bin" >> ~/.profile
fi
